이 group은 Frontend에 대한 포스팅의 모음이다.

### Basics
- [UX의 기본, Wireframe은 어떻게 작성하는 것일까?](https://brunch.co.kr/@second-space/27) 2024-12-03
- [PM에게 필요한 UIUX 개념과 구축 비용 총정리](https://brunch.co.kr/@a33f93b357b349e/32) 2024-12-06
- [IT 개발사 PM은 개발 기획을 이렇게 합니다](https://brunch.co.kr/@a33f93b357b349e/33) 2024-12-12
- [프론트엔트 개발자는 서비스말고 '운영툴'도 만듭니다](https://yozm.wishket.com/magazine/detail/2519/) 2024-12-17


### Published
- [2023년 웹 개발을 위한 최고의 프론트엔드 프레임워크](https://frontend-techs.gitlab.io/best-frontend-frameworks-in-2023) 23-12-07
- [SvelteAPI와 FastAPI로 세련된 Hello World를 구축하기 단계별 가이드](https://frontend-techs.gitlab.io/building-stylish-hello-world-with-svelte-and-fastapi/) 23-12-08
- [Svelte](https://developer.mozilla.org/ko/docs/Learn/Tools_and_testing/Client-side_JavaScript_frameworks/Svelte_getting_started) 23-12-09
- [개발자를 위한 Figma 사용 방법과 활용 팁](https://yozm.wishket.com/magazine/detail/2802/?utm_source=stibee&utm_medium=email&utm_campaign=newsletter_yozm&utm_content=contents) 2025-01-17


### Under Working
- [Javascript Tutorial](https://gitlab.com/frontend-techs/javascript-tutorial/) 2024-10-18

### Waiting
- [Svelte Js Tutorial: Building a simple web app (a click-to-tweet generator)](https://medium.com/the-research-nest/how-to-build-a-simple-web-app-a-click-to-tweet-generator-in-svelte-and-javascript-cc689bc1bf84)
- [5 Exceptional Python Frameworks for Frontend Development](https://python.plainenglish.io/5-exceptional-python-frameworks-for-frontend-development-fcb7abb87462) 2024-02-02
